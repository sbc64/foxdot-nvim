
GO_PATH := $(shell go env GOPATH)

build:
	go build -v -o bin/foxdot cmd/foxdot/main.go

install: build
	cp ./bin/foxdot ~/.local/bin/foxdot

package foxdot

import (
	"strings"

	"github.com/neovim/go-client/nvim/plugin"
)

func hello(args []string) (string, error) {
	return "Hello " + strings.Join(args, " "), nil
}

// Foxdot is the main package caller
func Foxdot() {
	plugin.Main(func(p *plugin.Plugin) error {
		p.HandleFunction(&plugin.FunctionOptions{Name: "Hello"}, hello)
		return nil
	})
}
